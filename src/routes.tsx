import React from 'react';
import { Redirect } from 'react-router';

import asyncComponent from 'src/common/components/async-component';
import { Route, route } from 'src/common/lib/utils';

export const moduleName = 'netspeed';

const Dashboard = asyncComponent(() => import('./containers/graph-dashboard-page'));
const AdminPage = asyncComponent(() => import('./containers/admin-page'));

export const routes: Array<Route<any, any>> = [
    route('/', 'home', Dashboard, 'home', { exact: true }),
    route('/admin', 'query', () => <Redirect to="/admin/1" />, 'admin', { exact: true }),
    route('/admin/:mac(mac:[A-F0-9a-f:]{1,17}/)?:page', 'query', AdminPage, 'adminFull', { hide: true }),
    route('/:device', 'home', Dashboard, 'homeFull', { hide: true }),
];