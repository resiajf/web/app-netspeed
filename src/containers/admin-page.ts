import { connect } from 'react-redux';

import { clearError } from 'src/common/redux/errors/actions';
import { AdminPage } from 'src/components/pages/admin-page';
import { AdminPageDispatchToProps, AdminPageStateToProps } from 'src/components/pages/admin-page.interfaces';
import { State } from 'src/redux';
import { deleteDevice, loadAllDevices, unloadDevices, unsetUpdating } from 'src/redux/devices/actions';

const mapStateToProps = ({ devices, errors }: State): AdminPageStateToProps => ({
    devices: devices.devices[devices.page || 1],
    loading: devices.loading,
    notFoundError: errors.notFound,
    serverError: errors.serverUnavailable,
    totalPages: devices.totalPages,
    updating: devices.updating,
});

const mapDispatchToProps = (dispatch: any): AdminPageDispatchToProps => ({
    clearError: () => dispatch(clearError()),
    load: (page, mac) => dispatch(loadAllDevices(page, mac)),
    removeDevice: (device) => dispatch(deleteDevice(device)),
    unload: () => dispatch(unloadDevices()),
    unsetUpdating: () => dispatch(unsetUpdating()),
});

export default connect(mapStateToProps, mapDispatchToProps)(AdminPage);