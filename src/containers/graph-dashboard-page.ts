import { connect } from 'react-redux';

import { clearError } from 'src/common/redux/errors/actions';
import GraphDashboardPage from 'src/components/pages/graph-dashboard-page';
import {
    GraphDashboardPageDispatchToProps,
    GraphDashboardPageStateToProps
} from 'src/components/pages/graph-dashboard-page.interfaces';
import { State } from 'src/redux';
import { loadMyDevices, unloadDevices, unsetLoading } from 'src/redux/devices/actions';

const mapStateToProps = ({ devices, errors }: State): GraphDashboardPageStateToProps => ({
    devices: devices.devices[1] || [],
    loading: devices.loading,
    notFoundError: errors.notFound,
    serverUnavailableError: errors.serverUnavailable,
});

const mapDispatchToProps = (dispatch: any): GraphDashboardPageDispatchToProps => ({
    clearError: () => dispatch(clearError()),
    clearLoading: () => dispatch(unsetLoading()),
    load: () => dispatch(loadMyDevices()),
    unload: () => dispatch(unloadDevices()),
});

export default connect(mapStateToProps, mapDispatchToProps)(GraphDashboardPage);