import moment from 'moment';
import React from 'react';
import { InjectedTranslateProps, translate } from 'react-i18next';

import { Device, NetworkSpeed } from 'src/common/lib/api';

import 'metrics-graphics/dist/metricsgraphics.css';
const MG = require('metrics-graphics');
const $ = require('jquery');

interface MonthNetworkGraphProps extends InjectedTranslateProps {
    device: Device;
    fill: boolean;
}

interface MonthNetworkGraphState {
    dataAvg: Array<[ Date, number | null, number | null ]>;
    dataTotal: Array<{ time: Date, download: number | null, upload: number | null }>;
    loading: boolean;
}

const formatSpeed = (y: number | null, fraction: number = 2) => {
    if(y === null) {
        return `N/A`;
    }
    if(y < 1000) {
        return `${y}B/s`;
    } else if(y < 1000000) {
        return `${(y / 1000).toFixed(fraction)}KB/s`;
    } else if(y < 1000000000) {
        return `${(y / 1000000).toFixed(fraction)}MB/s`;
    } else {
        return `${(y / 1000000000).toFixed(fraction)}GB/s`;
    }
};

class MonthNetworkGraphClass extends React.Component<MonthNetworkGraphProps, MonthNetworkGraphState> {

    private graphRef = React.createRef<HTMLDivElement>();

    constructor(props: MonthNetworkGraphProps) {
        super(props);

        this.state = {
            dataAvg: [ [ new Date(), 0, 0 ] ],
            dataTotal: [ { time: new Date(), download: 0, upload: 0 } ],
            loading: false,
        };

        this.renderGraph = this.renderGraph.bind(this);
    }

    public componentDidMount() {
        //TODO
        this.loadData();

        $(window).resize(this.renderGraph);
    }

    public componentDidUpdate(prevProps: Readonly<MonthNetworkGraphProps>, prevState: Readonly<MonthNetworkGraphState>) {
        if(prevState.loading && !this.state.loading) {
            this.renderGraph();
        }

        if(this.props.fill !== prevProps.fill || prevProps.device.mac !== this.props.device.mac) {
            this.loadData();
        }
    }

    public componentWillUnmount() {
        $(window).off('resize', window, this.renderGraph);
    }

    public render() {
        return (
            <div className="mb-4" style={{ opacity: this.state.loading ? 0.6 : 1 }}>
                <div style={{ width: '100%' }} ref={ this.graphRef } />
            </div>
        );
    }

    private async loadData() {
        this.setState({ loading: true });
        try {
            const dataRaw = await NetworkSpeed.month(this.props.device, this.props.fill);
            const data = dataRaw
                .reverse()
                .map(({ time, avg_download, avg_upload, total_download, total_upload }) => [
                    new Date(time),
                    avg_download,
                    avg_upload !== null ? -avg_upload : null,
                    total_download,
                    total_upload !== null ? -total_upload : null,
                ]);
            this.setState({
                dataAvg: data.map(([ time, download, upload ]) => [ time, download, upload ]) as any,
                dataTotal: data.map(([ time, _1, _2, download, upload ]) => ({ time, download, upload })) as any,
                loading: false,
            });
        } catch(e) {
            //TODO
            this.setState({
                loading: false,
            });
        }
    }

    private valueFormatter(kind: string): (y: any) => string {
        if(kind === this.props.t('graph.avg_download')) {
            return (y: number) => `<span class="text-primary">${formatSpeed(y)}</span>`;
        } else if(kind === this.props.t('graph.avg_upload')) {
            return (y: number) => `<span class="text-danger">${formatSpeed(-y)}</span>`;
        } else if(kind === 'x') {
            return (x: Date) => moment(x).format('DD MMM');
        } else {
            return (y: number) => `<span class="sr-only">${y}</span>`;
        }
    }

    private renderGraph() {
        const data = this.state.dataTotal.map(({ time, download, upload }: any) => ({ total: download - upload, time }));
        MG.data_graphic({
            binned: true,
            chart_type: 'histogram',
            data,
            height: 400,
            mouseover: ({ x, y }: any, i: number) => {
                const date = this.valueFormatter('x')(x);
                const download = formatSpeed(this.state.dataTotal[i].download);
                const upload = formatSpeed(this.state.dataTotal[i].upload !== null ? -this.state.dataTotal[i].upload! : null);
                const total = formatSpeed(y);
                $(this.graphRef.current!)
                    .find('svg .mg-active-datapoint')
                    .html(`${date} | Total ${total} (↓ ${download} | ↑ ${upload})`);
            },
            target: this.graphRef.current!,
            width: $(this.graphRef.current).width(),
            x_accessor: 'time',
            y_accessor: 'total',
        });
    }

}

export const MonthNetworkGraph = translate()(MonthNetworkGraphClass);
