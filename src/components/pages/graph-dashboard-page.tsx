import HelpIcon from '@material-ui/icons/HelpOutline';
import React from 'react';
import { Trans, translate } from 'react-i18next';
import { NavLink } from 'react-router-dom';
import { toast } from 'react-toastify';

import { Button } from 'src/common/components/bootstrap/buttons';
import { Modal, ModalBody, ModalHeader } from 'src/common/components/bootstrap/modal';
import { Device, Devices } from 'src/common/lib/api';
import { localStorage } from 'src/common/lib/navigator-storage';
import { debounce } from 'src/common/lib/utils';
import { Form } from 'src/components/graph-panel-form';
import { MonthNetworkGraph } from 'src/components/month-network-graph';
import { GraphDashboardPageProps } from 'src/components/pages/graph-dashboard-page.interfaces';
import { RealTimeNetworkGraph } from 'src/components/realtime-network-graph';
import { WeekNetworkGraph } from 'src/components/week-network-graph';

import 'src/styles/graph-dashboard.css';

interface GraphDashboardPageState {
    selectedDevice: Device | null;
    from: number;
    to: number;
    fill: boolean;
    helpModal: boolean;
}

class GraphDashboardPageClass extends React.Component<GraphDashboardPageProps, GraphDashboardPageState> {

    private readonly changeRangeDebounced: (from: number, to: number) => void;

    constructor(props: GraphDashboardPageProps) {
        super(props);

        this.state = {
            fill: localStorage.getItem('netspeed:fill', true),
            from: localStorage.getItem('netspeed:from', 900),
            helpModal: false,
            selectedDevice: null,
            to: localStorage.getItem('netspeed:to', 0),
        };

        this.toggleHelpModal = this.toggleHelpModal.bind(this);
        this.closedHelpModal = this.closedHelpModal.bind(this);
        this.changeRange = this.changeRange.bind(this);

        this.changeRangeDebounced = debounce(this.changeRange, 1500);
    }

    public componentDidMount() {
        this.props.load();
        if(!isNaN(Number(this.props.match.params.device))) {
            //This code allows you to see another user's device (given by its ID)
            (async () => {
                try {
                    this.setState({
                        selectedDevice: await Devices.getOne(Number(this.props.match.params.device)),
                    });
                } catch(e) {
                    toast.error(this.props.t('could_not_load_only') + ' ' + this.props.t(e.translatedMessageKey || 'undefined'));
                    this.props.history.goBack();
                }
            })();
        }
    }

    public componentDidUpdate(prevProps: Readonly<GraphDashboardPageProps>) {
        if((prevProps.loading && !this.props.loading && !this.props.serverUnavailableError) || prevProps.match.params.device !== this.props.match.params.device) {
            if(this.props.match.params.device) {
                try {
                    const moc = atob(this.props.match.params.device!);
                    const device = this.props.devices.find(d => d.mac.toLowerCase() === moc.toLowerCase());
                    if(device) {
                        this.setState({
                            selectedDevice: device,
                        });
                    } else {
                        this.props.history.push('/');
                    }
                } catch(e) {
                    //Only if the device is not base64 MAC or a device ID
                    if(isNaN(Number(this.props.match.params.device))) {
                        //not a base 64 text
                        this.props.history.push('/');
                    }
                }
            } else {
                this.setState({
                    selectedDevice: null,
                });
            }
        }

        if(!prevProps.serverUnavailableError && this.props.serverUnavailableError) {
            setTimeout(this.props.clearError);
            this.props.clearLoading();
        }

        if(!prevProps.notFoundError && this.props.notFoundError) {
            setTimeout(this.props.clearError);
            this.props.clearLoading();
            toast.error(<p>
                { this.props.t('error.not-found') }
                <small>{ this.props.t(this.props.notFoundError.translatedMessageKey) }</small>
            </p>);
            if(this.props.history.length > 1) {
                this.props.history.goBack();
            }
        }
    }

    public componentWillUnmount() {
        this.props.unload();
    }

    public render() {
        const { devices, loading, t } = this.props;
        const { fill, from, helpModal, selectedDevice, to } = this.state;

        if(loading) {
            return (
                <h1 className="text-center">
                    { t('loading') }
                </h1>
            );
        } else if(!loading && devices.length === 0) {
            return (
                <>
                    <h1 className="text-center">
                        { t('no_devices') }
                    </h1>
                    <p className="text-muted">
                        { t('help.content.1') }
                    </p>
                </>
            );
        }

        let content;
        if(selectedDevice) {
            content = (
                <>
                    <p className="mt-2">
                        { t('last_known_domain') } <code>{ selectedDevice.last_known_hostname || t('last_known_domain_unknown') }</code>
                        <br/>
                        { t('last_known_ip') } <code>{ selectedDevice.last_known_ip_address || t('last_known_ip_unknown') }</code>
                        <br/>
                        { t('mac') } <code>{ selectedDevice.mac }</code>
                    </p>
                    <h2>{ t('realtime_graph') }</h2>
                    <RealTimeNetworkGraph device={ selectedDevice } fill={ fill } from={ from } to={ to } />
                    <Form from={ from } to={ to } t={ t } onChange={ this.changeRangeDebounced } />
                    <hr/>
                    <h2>{ t('week_graph') }</h2>
                    <WeekNetworkGraph device={ selectedDevice } fill={ fill } />
                    <hr/>
                    <h2>{ t('month_graph') }</h2>
                    <MonthNetworkGraph device={ selectedDevice } fill={ fill } />
                </>
            );
        } else {
            content = <p className="lead text-center mt-2">{ t('select_one') }</p>;
        }

        return (
            <>
                <h1 className="d-flex align-items-center">
                    { t('title') }
                    <small>
                        <Button type="link" onClick={ this.toggleHelpModal }>
                            <HelpIcon className="text-muted" />
                        </Button>
                    </small>
                </h1>
                <div className="alert alert-warning d-block d-lg-none" role="alert">
                    { t('small_screen_warning') }
                </div>
                <nav className="graph-tables">
                    <div className="nav nav-tabs" role="tablist">
                        { devices.map((device, i) =>
                            <NavLink to={ `/${btoa(device.mac)}` } className="nav-item nav-link" role="tab" key={ i }>
                                { device.last_known_hostname || device.last_known_ip_address || device.mac }
                            </NavLink>
                        ) }
                    </div>
                </nav>
                { content }

                <Modal show={ helpModal } onClosed={ this.closedHelpModal }>
                    <ModalHeader title={ t('help.title') } onClose={ this.toggleHelpModal } />
                    <ModalBody>
                        <p>{ t('help.content.0') }</p>
                        <p><Trans i18nKey="help.content.1">. <a href="/computer-issues/create">.</a> .</Trans></p>
                        <p><Trans i18nKey="help.content.2">. <code>.</code> . <code>.</code> . <code>.</code> .</Trans></p>
                        <p><Trans i18nKey="help.content.3">.</Trans></p>
                        <p><Trans i18nKey="help.content.4">. <i>.</i></Trans></p>
                    </ModalBody>
                </Modal>
            </>
        );
    }

    private toggleHelpModal(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        this.setState({
            helpModal: !this.state.helpModal,
        });
    }

    private closedHelpModal() {
        this.setState({
            helpModal: false,
        });
    }

    private changeRange(from: number, to: number) {
        this.setState({
            from,
            to,
        }, () => {
            localStorage.setItem('netspeed:from', from);
            localStorage.setItem('netspeed:to', to);
        });
    }

}

export default translate()(GraphDashboardPageClass);
