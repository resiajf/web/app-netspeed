import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import OpenInBrowserIcon from '@material-ui/icons/OpenInBrowser';
import React from 'react';
import { translate } from 'react-i18next';
import { Link, Redirect } from 'react-router-dom';
import { toast } from 'react-toastify';

import { Button } from 'src/common/components/bootstrap/buttons';
import Pagination from 'src/common/components/pagination';
import { Device } from 'src/common/lib/api';
import { AdminFilter } from 'src/components/admin-filter';
import { AdminPageProps } from 'src/components/pages/admin-page.interfaces';

import 'src/styles/admin-page.css';

class AdminPageClass extends React.Component<AdminPageProps> {

    private removeNotification: number | null;

    constructor(props: AdminPageProps) {
        super(props);

        this.onFilterChange = this.onFilterChange.bind(this);
        this.onPageChange = this.onPageChange.bind(this);
    }

    public componentDidMount() {
        this.props.load(this.page, this.mac);
    }

    public componentDidUpdate(prevProps: Readonly<AdminPageProps>) {
        if(prevProps.location.pathname !== this.props.location.pathname) {
            this.props.load(this.page, this.mac);
        }

        if(prevProps.updating && !this.props.updating) {
            if(this.removeNotification) {
                toast.update(this.removeNotification, {
                    render: this.props.t('removed'),
                    type: toast.TYPE.SUCCESS,
                });
                this.removeNotification = null;
            }
        }

        if(!prevProps.notFoundError && this.props.notFoundError) {
            if(this.removeNotification) {
                toast.update(this.removeNotification, {
                    render: <p>{ this.props.t('no_removed') }<br/><small>{ this.props.t(this.props.notFoundError.translatedMessageKey) }</small></p>,
                    type: toast.TYPE.ERROR,
                });
                this.removeNotification = null;
            }
            this.props.unsetUpdating();
            this.props.clearError();
        }

        if(!prevProps.serverError && this.props.serverError) {
            this.props.unsetUpdating();
            setTimeout(this.props.clearError);
        }
    }

    public componentWillUnmount() {
        this.props.unload();
    }

    public render() {
        const { devices, totalPages, t } = this.props;

        if((!devices || devices.length === 0) && !this.props.loading && totalPages !== undefined && this.page > totalPages) {
            return <Redirect to={ `/admin/${this.props.match.params.mac || ''}${totalPages}` } />;
        }

        let content;
        if(devices) {
            content = devices.map((device, i) => (
                <tr key={ i }>
                    <th scope="row">{ device.id_dispositivo }</th>
                    <td><code>{ device.mac }</code></td>
                    <td><code>{ device.last_known_ip_address || t('last_known_ip_unknown') }</code></td>
                    <td>{ device.last_known_hostname || t('last_known_domain_unknown') }</td>
                    <td>{ device.owner_display_name }</td>
                    <td>
                        <Link to={ `/${device.id_dispositivo}` } className="btn btn-outline-primary btn-sm mr-1"><OpenInBrowserIcon /></Link>
                        <Button type="danger" outline={ true } size="sm" onClick={ this.onDeleteDevice(device) }><DeleteForeverIcon /></Button>
                    </td>
                </tr>
            ));
        } else {
            content = <tr><th scope="row" colSpan={ 5 } className="text-center lead">{ t('loading') }</th></tr>;
        }

        return (
            <>
                <h1 className="display-4">{ t('admin_title') }</h1>
                <AdminFilter mac={ this.mac } onApplyFilter={ this.onFilterChange } onClearFilter={ this.onFilterChange } />

                <h4>{ t('admin_table_title') }</h4>
                <Pagination page={ this.page - 1 } pages={ totalPages || 1 } onChange={ this.onPageChange } />
                <div className="table-responsive devices-table">
                    <table className="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">MAC</th>
                            <th scope="col">IP</th>
                            <th scope="col">Hostname</th>
                            <th scope="col">{ t('owner') }</th>
                            <th />
                        </tr>
                        </thead>
                        <tbody>
                        { content }
                        </tbody>
                    </table>
                </div>
            </>
        );
    }

    private get page() {
        return Number(this.props.match.params.page);
    }

    private get mac() {
        const { mac } = this.props.match.params;
        if(mac) {
            return mac.substring(4, mac.length - 1);
        }
        return undefined;
    }

    private onPageChange(page: number) {
        const { mac } = this.props.match.params;
        this.props.history.push(`/admin/${mac || ''}${page + 1}`);
    }

    private onDeleteDevice(device: Device) {
        return (e: React.MouseEvent<HTMLButtonElement>) => {
            e.preventDefault();
            this.props.removeDevice(device);
            this.removeNotification = toast(this.props.t('removing'));
        };
    }

    private onFilterChange(mac?: string) {
        const macPart = mac ? `mac:${mac}/` : '';
        this.props.history.push(`/admin/${macPart}1`);
    }

}

export const AdminPage = translate()(AdminPageClass);
