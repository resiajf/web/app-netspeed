import { InjectedTranslateProps } from 'react-i18next';
import { RouteComponentProps } from 'react-router';

import { Device, NotFoundError, ServerUnavailableError } from 'src/common/lib/api';

export interface GraphDashboardPageStateToProps {
    devices: Device[];
    loading: boolean;
    notFoundError: NotFoundError | null;
    serverUnavailableError: ServerUnavailableError | null;
}

export interface GraphDashboardPageDispatchToProps {
    load: () => void;
    unload: () => void;
    clearLoading: () => void;
    clearError: () => void;
}

export type GraphDashboardPageOwnProps = InjectedTranslateProps & RouteComponentProps<{ device?: string }>;

export type GraphDashboardPageProps = GraphDashboardPageStateToProps & GraphDashboardPageDispatchToProps &
    GraphDashboardPageOwnProps;