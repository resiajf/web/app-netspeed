import { InjectedTranslateProps } from 'react-i18next';
import { RouteComponentProps } from 'react-router';

import { Device, NotFoundError, ServerUnavailableError } from 'src/common/lib/api';

export interface AdminPageStateToProps {
    devices?: Device[];
    loading: boolean;
    notFoundError: NotFoundError | null;
    serverError: ServerUnavailableError | null;
    updating: boolean;
    totalPages?: number;
}

export interface AdminPageDispatchToProps {
    load: (page: number, mac?: string) => void;
    removeDevice: (device: Device) => void;
    unload: () => void;
    unsetUpdating: () => void;
    clearError: () => void;
}

export type AdminPageOwnProps = InjectedTranslateProps & RouteComponentProps<{ page: string, mac?: string }>;

export type AdminPageProps = AdminPageStateToProps & AdminPageDispatchToProps & AdminPageOwnProps;