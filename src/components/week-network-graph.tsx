import moment from 'moment';
import React from 'react';
import { InjectedTranslateProps, translate } from 'react-i18next';

import { Device, NetworkSpeed } from 'src/common/lib/api';

import 'metrics-graphics/dist/metricsgraphics.css';
const { default: Dygraph } = require('dygraphs');
const MG = require('metrics-graphics');
const $ = require('jquery');

interface WeekNetworkGraphProps extends InjectedTranslateProps {
    device: Device;
    fill: boolean;
}

interface WeekNetworkGraphState {
    dataAvg: Array<[ Date, number | null, number | null ]>;
    dataTotal: Array<{ time: Date, download: number | null, upload: number | null }>;
    loading: boolean;
}

const formatSpeed = (y: number | null, fraction: number = 2) => {
    if(y === null) {
        return `N/A`;
    }
    if(y < 1000) {
        return `${y}B/s`;
    } else if(y < 1000000) {
        return `${(y / 1000).toFixed(fraction)}KB/s`;
    } else if(y < 1000000000) {
        return `${(y / 1000000).toFixed(fraction)}MB/s`;
    } else {
        return `${(y / 1000000000).toFixed(fraction)}GB/s`;
    }
};

class WeekNetworkGraphClass extends React.Component<WeekNetworkGraphProps, WeekNetworkGraphState> {

    private graphRef = React.createRef<HTMLDivElement>();
    private labelRef = React.createRef<HTMLDivElement>();
    private graph2Ref = React.createRef<HTMLDivElement>();
    private dygraph: any;

    constructor(props: WeekNetworkGraphProps) {
        super(props);

        this.state = {
            dataAvg: [ [ new Date(), 0, 0 ] ],
            dataTotal: [ { time: new Date(), download: 0, upload: 0 } ],
            loading: false,
        };

        this.renderSecondGraph = this.renderSecondGraph.bind(this);
    }

    public componentDidMount() {
        this.dygraph = new Dygraph(this.graphRef.current, this.state.dataAvg, {
            axes: {
                y: {
                    axisLabelFormatter: (x: number) => formatSpeed(Math.abs(x), 0),
                }
            },
            fillGraph: true,
            labels: [ 'x', this.props.t('graph.avg_download'), this.props.t('graph.avg_upload') ],
            labelsDiv: this.labelRef.current!,
            legend: 'always',
            legendFormatter: this.legendFormatter.bind(this),
            valueFormatter: (x: any, _: any , s: string) => this.valueFormatter(s)!(x),
        });
        this.loadData();

        $(window).resize(this.renderSecondGraph);
    }

    public componentDidUpdate(prevProps: Readonly<WeekNetworkGraphProps>, prevState: Readonly<WeekNetworkGraphState>) {
        if(prevState.loading && !this.state.loading) {
            this.dygraph.updateOptions({
                file: this.state.dataAvg,
            });
            this.renderSecondGraph();
        }

        if(this.props.fill !== prevProps.fill || prevProps.device.mac !== this.props.device.mac) {
            this.loadData();
        }
    }

    public componentWillUnmount() {
        $(window).off('resize', window, this.renderSecondGraph);
    }

    public render() {
        return (
            <>
                <h4>{ this.props.t('week_avg_graph') }</h4>
                <div className="mb-4 text-center" style={{ opacity: this.state.loading ? 0.6 : 1 }}>
                    <div style={{ width: '100%' }} ref={ this.graphRef } />
                    <div style={{ minHeight: 48 }} ref={ this.labelRef } />
                </div>
                <h4>{ this.props.t('week_total_graph') }</h4>
                <div className="mb-4" style={{ opacity: this.state.loading ? 0.6 : 1 }}>
                    <div style={{ width: '100%' }} ref={ this.graph2Ref } />
                </div>
            </>
        );
    }

    private async loadData() {
        this.setState({ loading: true });
        try {
            const dataRaw = await NetworkSpeed.week(this.props.device, this.props.fill);
            const data = dataRaw
                .reverse()
                .map(({ time, avg_download, avg_upload, total_download, total_upload }) => [
                    new Date(time),
                    avg_download,
                    avg_upload !== null ? -avg_upload : null,
                    total_download,
                    total_upload !== null ? -total_upload : null,
                ]);
            this.setState({
                dataAvg: data.map(([time, download, upload]) => [time, download, upload]) as any,
                dataTotal: data.map(([time, _1, _2, download, upload]) => ({ time, download, upload })) as any,
                loading: false,
            });
        } catch(e) {
            //TODO
            this.setState({ loading: false });
        }
    }

    private valueFormatter(kind: string): (y: any) => string {
        if(kind === this.props.t('graph.avg_download')) {
            return (y: number) => `<span class="text-primary">${formatSpeed(y)}</span>`;
        } else if(kind === this.props.t('graph.avg_upload')) {
            return (y: number) => `<span class="text-danger">${formatSpeed(-y)}</span>`;
        } else if(kind === 'x') {
            return (x: Date) => moment(x).format('DD MMM - HH:mm');
        } else {
            return (y: number) => `<span class="sr-only">${y}</span>`;
        }
    }

    private legendFormatter(data: any) {
        if(!data.x) {
            return '';
        }

        const y1 = data.series[0].yHTML || `<span class="text-warning">${this.props.t('graph.no_value')}</span>`;
        const y2 = data.series[1].yHTML || `<span class="text-warning">${this.props.t('graph.no_value')}</span>`;

        return `${data.xHTML}<br>↓ ${y1} - ↑ ${y2}`;
    }

    private renderSecondGraph() {
        const data = this.state.dataTotal.map(({ time, download, upload }: any) => ({ total: download - upload, time }));
        MG.data_graphic({
            binned: true,
            chart_type: 'histogram',
            data,
            height: 400,
            mouseover: ({ x, y }: any, i: number) => {
                const date = this.valueFormatter('x')(x);
                const download = formatSpeed(this.state.dataTotal[i].download);
                const upload = formatSpeed(this.state.dataTotal[i].upload !== null ? -this.state.dataTotal[i].upload! : null);
                const total = formatSpeed(y);
                $(this.graph2Ref.current!)
                    .find('svg .mg-active-datapoint')
                    .html(`${date} | Total ${total} (↓ ${download} | ↑ ${upload})`);
            },
            target: this.graph2Ref.current!,
            width: $(this.graph2Ref.current).width(),
            x_accessor: 'time',
            y_accessor: 'total',
        });
    }

}

export const WeekNetworkGraph = translate()(WeekNetworkGraphClass);
