import React from 'react';
import { InjectedTranslateProps, translate } from 'react-i18next';

import { Button } from 'src/common/components/bootstrap/buttons';

interface AdminFilterProps extends InjectedTranslateProps {
    mac?: string;
    onApplyFilter: (mac: string) => void;
    onClearFilter: () => void;
}

interface AdminFilterState {
    mac: string;
}

class AdminFilterClass extends React.Component<AdminFilterProps, AdminFilterState> {

    private static macFilterRegex = /^[a-fA-F0-9]{2}(?::[a-fA-F0-9]{0,2}){0,5}$/;

    constructor(props: AdminFilterProps) {
        super(props);

        this.state = {
            mac: props.mac || '',
        };

        this.onClearFilterClicked = this.onClearFilterClicked.bind(this);
        this.onDoFilterClicked = this.onDoFilterClicked.bind(this);
        this.onMacChange = this.onMacChange.bind(this);
    }

    public componentDidUpdate(prevProps: Readonly<AdminFilterProps>) {
        if(prevProps.mac !== this.props.mac) {
            this.setState({
                mac: this.props.mac || '',
            });
        }
    }

    public render() {
        const { t } = this.props;
        const { mac } = this.state;

        const isInvalid = mac.length !== 0 && mac.match(AdminFilterClass.macFilterRegex) === null;

        return (
            <div className="mb-3">
                <h4>{ t('admin_filter.title') }</h4>
                <div className="form-group">
                    <label htmlFor="mac-filter">{ t('admin_filter.mac_label') }</label>
                    <input id="mac-filter"
                           className={ `form-control ${!isInvalid ? '' : 'is-invalid'}` }
                           maxLength={ 17 }
                           value={ mac }
                           onChange={ this.onMacChange } />
                    <small className="help-block text-muted">{ t('admin_filter.mac_help') }</small>
                </div>
                <Button type="primary"
                        size="sm"
                        className="mr-1"
                        disabled={ isInvalid || mac.length === 0 }
                        onClick={ this.onDoFilterClicked }>
                    { t('admin_filter.do_filter') }
                </Button>
                <Button type="secondary"
                        size="sm"
                        disabled={ !this.props.mac }
                        onClick={ this.onClearFilterClicked }>
                    { t('admin_filter.clear') }
                </Button>
            </div>
        );
    }

    private onMacChange(e: React.ChangeEvent<HTMLInputElement>) {
        this.setState({
            mac: e.target.value.replace(/[^a-fA-F0-9:]/, ''),
        });
    }

    private onDoFilterClicked(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        this.props.onApplyFilter(this.state.mac);
    }

    private onClearFilterClicked(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        this.props.onClearFilter();
    }

}

export const AdminFilter = translate()(AdminFilterClass);