import moment from 'moment';
import React from 'react';
import { InjectedTranslateProps, translate } from 'react-i18next';
import { toast } from 'react-toastify';

import { Device, NetworkSpeedRow } from 'src/common/lib/api';
import { InstantSpeedStream } from 'src/common/lib/api/network-speed/instant-speed-stream';

const { default: Dygraph } = require('dygraphs');

interface RealtimeNetworkGraphProps extends InjectedTranslateProps {
    device: Device;
    from: number;
    to: number;
    fill: boolean;
}

interface RealtimeNetworkGraphState {
    data?: Array<[ Date, number, number ]>;
}

const formatSpeed = (y: number, fraction: number = 2) => {
    if(y < 1000) {
        return `${y}B/s`;
    } else if(y < 1000000) {
        return `${(y / 1000).toFixed(fraction)}KB/s`;
    } else if(y < 1000000000) {
        return `${(y / 1000000).toFixed(fraction)}MB/s`;
    } else {
        return `${(y / 1000000000).toFixed(fraction)}GB/s`;
    }
};

class RealTimeNetworkGraphClass extends React.PureComponent<RealtimeNetworkGraphProps, RealtimeNetworkGraphState> {

    private readonly stream: InstantSpeedStream;
    private cleanUpFlag = true;
    private graphRef = React.createRef<HTMLDivElement>();
    private labelRef = React.createRef<HTMLDivElement>();
    private dygraph: any;

    constructor(props: RealtimeNetworkGraphProps) {
        super(props);

        this.state = {
            data: undefined,
        };

        this.stream = new InstantSpeedStream(props.device, props.from, props.to, props.fill);
    }

    public componentDidMount() {
        this.stream
            .on('data', (data: NetworkSpeedRow[]) => {
                const transformedData: Array<[ Date, number, number ]> = data
                    .map(({ time, download, upload }): [ Date, number, number ] => [ new Date(time), download, -upload ])
                    .reverse();
                if(this.cleanUpFlag) {
                    this.cleanUpFlag = false;
                    this.setState({
                        data: transformedData,
                    });
                } else {
                    this.setState({
                        data: [ ...this.state.data!.slice(1), ...transformedData ],
                    });
                }
            })
            .on('reset', () => {
                this.cleanUpFlag = true;
            })
            .on('error', (e: any) => {
                if(e.status === 404) {
                    toast.error(<p>
                        { this.props.t('graph.error404_realtime') }
                        <small>{ this.props.t(e.translatedMessageKey) }</small>
                    </p>);
                } else {
                    console.error(e);
                }
            });

        this.dygraph = new Dygraph(this.graphRef.current, [[ new Date('1970-01-01'), 0, 0 ]], {
            axes: {
                y: {
                    axisLabelFormatter: (x: number) => formatSpeed(Math.abs(x), 0),
                }
            },
            fillGraph: true,
            labels: ['x', this.props.t('graph.download'), this.props.t('graph.upload')],
            labelsDiv: this.labelRef.current!,
            legend: 'always',
            legendFormatter: this.legendFormatter.bind(this),
            valueFormatter: (x: any, _: any , s: string) => this.valueFormatter(s)!(x),
        });

        this.stream.stop = this.stream.stop.bind(this.stream);
        this.stream.start = this.stream.start.bind(this.stream);
        window.addEventListener('blur', this.stream.stop);
        window.addEventListener('focus', this.stream.start);

        if(document.hasFocus()) {
            this.stream.start();
        }
    }

    public componentDidUpdate(prevProps: Readonly<RealtimeNetworkGraphProps>, prevState: Readonly<RealtimeNetworkGraphState>) {
        if(!prevState.data && this.state.data) {
            this.renderGraph();
        } else if(prevState.data && prevState.data[0][0] !== this.state.data![0][0]) {
            this.renderGraph();
        }

        if(prevProps.fill !== this.props.fill) {
            this.stream.fillWith0 = this.props.fill;
        }
        if(prevProps.from !== this.props.from) {
            this.stream.fromSeconds = this.props.from;
        }
        if(prevProps.to !== this.props.to) {
            this.stream.toSeconds = this.props.to;
        }
        if(prevProps.device.mac !== this.props.device.mac) {
            this.stream.device = this.props.device;
        }
    }

    public componentWillUnmount() {
        this.stream.stop();
        this.dygraph.destroy();
        window.removeEventListener('blur', this.stream.stop);
        window.removeEventListener('focus', this.stream.start);
    }

    public render() {
        return (
            <div className="mb-4 text-center">
                <div style={{ width: '100%' }} ref={ this.graphRef } />
                <div style={{ minHeight: 48 }} ref={ this.labelRef } />
            </div>
        );
    }

    private renderGraph() {
        this.dygraph.updateOptions({
            file: this.state.data
        });
    }

    private valueFormatter(kind: string): (y: any) => string {
        if(kind === this.props.t('graph.download')) {
            return (y: number) => `<span class="text-primary">${formatSpeed(y)}</span>`;
        } else if(kind === this.props.t('graph.upload')) {
            return (y: number) => `<span class="text-danger">${formatSpeed(-y)}</span>`;
        } else if(kind === 'x') {
            return (x: Date) => moment(x).format('HH:mm:ss');
        } else {
            return (y: number) => `<span class="sr-only">${y}</span>`;
        }
    }

    private legendFormatter(data: any) {
        if(!data.x) {
            return '';
        }

        const y1 = data.series[0].yHTML || `<span class="text-warning">${this.props.t('graph.no_value')}</span>`;
        const y2 = data.series[1].yHTML || `<span class="text-warning">${this.props.t('graph.no_value')}</span>`;

        return `${data.xHTML}<br>↓ ${y1} - ↑ ${y2}`;
    }

}

export const RealTimeNetworkGraph = translate()(RealTimeNetworkGraphClass);
