import React from 'react';
import { InjectedTranslateProps } from 'react-i18next';
import RangeInput from 'react-input-range';

import 'react-input-range/lib/css/index.css';

interface FormProps extends InjectedTranslateProps {
    from: number;
    to: number;
    onChange?: (from: number, to: number) => void;
}

export class Form extends React.Component<FormProps, any> {

    private static formatLabel(value: number) {
        const fmt = (n: number) => n < 10 ? `0${n}` : String(n);
        value = -value;
        if(value < 60) {
            return `00:00:${fmt(value)}`;
        } else if(value < 3600) {
            return `00:${fmt(Math.trunc(value / 60))}:${fmt(value % 60)}`;
        } else {
            return `${fmt(Math.trunc(value / 3600))}:${fmt(Math.trunc(value / 60) % 60)}:${fmt(value % 60)}`;
        }
    }

    constructor(props: FormProps) {
        super(props);

        this.state = {
            from: this.props.from,
            max: this.calculateMax(this.props.from, this.props.to),
            min: this.calculateMin(this.props.to, this.props.from),
            to: this.props.to,
        };

        this.onRangeChangeComplete = this.onRangeChangeComplete.bind(this);
        this.onRangeChange = this.onRangeChange.bind(this);
    }

    public componentDidMount() {
        this.setState({
            from: this.props.from,
            max: this.calculateMax(this.props.from, this.props.to),
            min: this.calculateMin(this.props.to, this.props.from),
            to: this.props.to,
        });
    }

    public componentDidUpdate(prevProps: Readonly<FormProps>) {
        if(prevProps.from !== this.props.from) {
            this.setState({
                from: this.props.from,
                max: this.calculateMax(this.props.from),
                min: this.calculateMin(this.props.to, this.props.from),
            });
        }

        if(prevProps.to !== this.props.to) {
            this.setState({
                max: this.calculateMax(this.props.from, this.props.to),
                min: this.calculateMin(this.props.to),
                to: this.props.to,
            });
        }
    }

    public render() {
        const { from, max, min, to } = this.state;

        return (
            <>
                <label htmlFor="rangeFrom">{ this.props.t('graph.range_label') }</label>
                <div style={{ marginLeft: 40, marginRight: 40, marginBottom: 25, marginTop: 10 }}>
                    <RangeInput maxValue={ -min }
                                minValue={ -max }
                                step={ -30 }
                                value={{ min: -from, max: -to }}
                                name="rangeFrom"
                                formatLabel={ Form.formatLabel }
                                onChange={ this.onRangeChange }
                                onChangeComplete={ this.onRangeChangeComplete } />
                </div>
            </>
        );
    }

    private onRangeChange({ max, min }: any) {
        this.setState({
            from: Math.max(this.state.to + 30, Math.min(-Number(min), 86400)),
            to: Math.max(0, Math.min(-Number(max), this.state.from - 30)),
        });
    }

    private onRangeChangeComplete() {
        this.setState({
            max: this.calculateMax(),
            min: this.calculateMin(),
        });

        if(this.props.onChange) {
            this.props.onChange(this.state.from, this.state.to);
        }
    }

    private calculateMax(from: number = this.state.from, to: number = this.state.to) {
        let a;
        if(from - to < 5 * 60) {
            a = 5;
        } else if(from - to < 15 * 60) {
            a = 15;
        } else if(from - to < 30 * 60) {
            a = 30;
        } else if(from - to < 45 * 60) {
            a = 45;
        } else {
            a = 60;
        }
        return Math.min(3600 * 24, from + 60*a);
    }

    private calculateMin(to: number = this.state.to, from: number = this.state.from) {
        let a;
        if(from - to < 5 * 60) {
            a = 5;
        } else if(from - to < 15 * 60) {
            a = 15;
        } else if(from - to < 30 * 60) {
            a = 30;
        } else if(from - to < 45 * 60) {
            a = 45;
        } else {
            a = 60;
        }
        return Math.max(0, to - 60*a);
    }


}