import { Device } from 'src/common/lib/api';
import {
    Actions,
    CLEAR_DEVICES,
    LOADED_DEVICES,
    LOADING,
    REMOVE_DEVICE,
    UNSET_LOADING, UNSET_UPDATING, UPDATING
} from 'src/redux/devices/actions';
import { DevicesState } from 'src/redux/devices/state';

const findDevice = (devices: Device[][], device: Device): [ number | null, number | null ] => {
    for(let i = 1; i < devices.length; i++) {
        if(devices[i] !== undefined) {
            const j = devices[i].findIndex((deviceAgain) => deviceAgain.mac.toLowerCase() === device.mac.toLowerCase());
            if(j !== -1) {
                return [i, j];
            }
        }
    }
    return [ null, null ];
};

export default (state: DevicesState, action: Actions) => {
    switch(action.type) {
        case CLEAR_DEVICES: {
            return {
                ...state,
                devices: [],
                loading: false,
                page: undefined,
                totalPages: undefined,
            };
        }

        case LOADED_DEVICES: {
            const devices = state.devices;
            devices[action.page] = action.devices;
            return {
                ...state,
                devices,
                loading: false,
                page: action.page,
                totalPages: action.totalPages,
            };
        }

        case LOADING: {
            return {
                ...state,
                loading: true,
            };
        }

        case REMOVE_DEVICE: {
            const [ page ] = findDevice(state.devices, action.device);
            if(page !== null) {
                let { totalPages } = state;
                if(page === state.totalPages && state.devices[page].length === 1) {
                    //If we are in the last page, and it will not have anymore notifications, reduce the total pages
                    totalPages = totalPages! - 1;
                }
                return {
                    ...state,
                    devices: [
                        ...state.devices.slice(0, page),
                        //Invalidate the rest of the cache
                    ],
                    totalPages,
                    updating: false,
                };
            }
            return state;
        }

        case UNSET_LOADING: {
            return {
                ...state,
                loading: false,
            };
        }

        case UNSET_UPDATING: {
            return {
                ...state,
                updating: false,
            };
        }

        case UPDATING: {
            return {
                ...state,
                updating: true,
            };
        }

        default: {
            return state || {
                devices: [],
                loading: false,
                page: undefined,
                totalPages: undefined,
                updating: false,
            };
        }
    }
};