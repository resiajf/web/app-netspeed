import { Device } from 'src/common/lib/api';

export interface DevicesState {
    devices: Device[][];
    page?: number;
    totalPages?: number;
    loading: boolean;
    updating: boolean;
}