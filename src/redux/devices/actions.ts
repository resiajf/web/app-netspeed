import { Action, Dispatch } from 'redux';

import { Device, Devices, ListDevices } from 'src/common/lib/api';
import { catchErrors as f } from 'src/common/redux/errors/decorator';

export const LOADING = 'devices:LOADING';
export const UNSET_LOADING = 'devices:UNSET_LOADING';
export const LOADED_DEVICES = 'devices:LOADED_DEVICES';
export const CLEAR_DEVICES = 'devices:CLEAR_DEVICES';
export const UPDATING = 'devices:UPDATING';
export const UNSET_UPDATING = 'devices:UNSET_UPDATING';
export const REMOVE_DEVICE = 'devices:REMOVE_DEVICE';

type LoadingAction = Action<typeof LOADING>;
type UnsetLoadingAction = Action<typeof UNSET_LOADING>;
type ClearDevicesAction = Action<typeof CLEAR_DEVICES>;
type UpdatingAction = Action<typeof UPDATING>;
type UnsetUpdatingAction = Action<typeof UNSET_UPDATING>;

interface LoadedDevicesAction extends Action<typeof LOADED_DEVICES> {
    devices: Device[];
    page: number;
    totalPages: number;
}

interface RemoveDeviceAction extends Action<typeof REMOVE_DEVICE> {
    device: Device;
}

export type Actions = LoadingAction | UnsetLoadingAction | ClearDevicesAction | UpdatingAction | UnsetUpdatingAction |
    LoadedDevicesAction | RemoveDeviceAction;

export const loadMyDevices = () => f(async (dispatch: Dispatch) => {
    dispatch({ type: LOADING });
    const res = await Devices.getList();
    let devices = res.results;
    let promises: Array<Promise<ListDevices>> = [];
    for(let i = 2; i <= res.pages; i++) {
        promises = promises.concat(Devices.getList(i));
    }
    for(const promise of promises) {
        devices = [ ...devices, ...((await promise).results) ];
    }
    dispatch({
        devices,
        page: 1,
        totalPages: 1,
        type: LOADED_DEVICES,
    });
});

export const loadAllDevices = (page: number, mac?: string) => f(async (dispatch: Dispatch) => {
    dispatch({ type: LOADING });
    const res = await Devices.getList(page, true, mac);
    dispatch({
        devices: res.results,
        page,
        totalPages: res.pages,
        type: LOADED_DEVICES,
    });
});

export const deleteDevice = (theDevice: Device) => f(async (dispatch: Dispatch) => {
    dispatch({ type: UPDATING });
    const device = await Devices.removeOne(theDevice);
    dispatch({
        device,
        type: REMOVE_DEVICE,
    });
});

export const unsetLoading = () => ({
    type: UNSET_LOADING,
});

export const unsetUpdating = () => ({
    type: UNSET_UPDATING,
});

export const unloadDevices = () => ({
    type: CLEAR_DEVICES,
});