import { combineReducers } from 'redux';

import { reducers as commonReducers, State as CommonState } from 'src/common/redux';
import devices from 'src/redux/devices/reducer';
import { DevicesState } from 'src/redux/devices/state';

export const reducers = combineReducers<State>({
    ...commonReducers,
    devices,
});

// tslint:disable-next-line:no-empty-interface
export interface State extends CommonState {
    devices: DevicesState;
}
